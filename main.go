/*
Copyright © 2023 NAME HERE <erudinsky@gitlab.com>
*/
package main

import "gitlab.com/evgenyrudinsky/hanoi/cmd"

func main() {
	cmd.Execute()
}
