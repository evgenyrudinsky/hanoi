[![Pipeline status](https://gitlab.com/evgenyrudinsky/hanoi/badges/main/pipeline.svg)](https://gitlab.com/evgenyrudinsky/hanoi/-/pipelines)
[![Release status](https://gitlab.com/evgenyrudinsky/hanoi/-/badges/release.svg)](https://gitlab.com/evgenyrudinsky/hanoi/-/releases)

# Hanoi

**Hanoi** app is a simple CLI application written in go that is here to help solving [Tower of Hanoi](https://en.wikipedia.org/wiki/Tower_of_Hanoi) problem. 

![](.attachments/Tower_of_Hanoi_4.gif)

## How to use

The tool can be executed without any flags and will show how to solve the problem with 3 disks. The tool expects `int` for the size and can be up to `9,223,372,036,854,775,807` (*nine quintillion two hundred twenty three quadrillion three hundred seventy two trillion thirty six billion eight hundred fifty four million seven hundred seventy five thousand eight hundred and seven*).

Examples of use: 

```bash

# Run without any flags shows how to solve the problem with 3 disks:

./hanoi howto

All right, to solve the problem with 3 disks you would need to make the following moves: 

Move disk 1 from 1 to 2
Move disk 2 from 1 to 3
Move disk 1 from 2 to 3
Move disk 3 from 1 to 2
Move disk 1 from 3 to 1
Move disk 2 from 3 to 2
Move disk 1 from 1 to 2

# For 5 disks:

./hanoi howto --size=5

All right, to solve the problem with 5 disks you would need to make the following moves: 

Move disk 1 from 1 to 2
Move disk 2 from 1 to 3
Move disk 1 from 2 to 3
Move disk 3 from 1 to 2
Move disk 1 from 3 to 1
Move disk 2 from 3 to 2
Move disk 1 from 1 to 2
Move disk 4 from 1 to 3
Move disk 1 from 2 to 3
Move disk 2 from 2 to 1
Move disk 1 from 3 to 1
Move disk 3 from 2 to 3
Move disk 1 from 1 to 2
Move disk 2 from 1 to 3
Move disk 1 from 2 to 3
Move disk 5 from 1 to 2
Move disk 1 from 3 to 1
Move disk 2 from 3 to 2
Move disk 1 from 1 to 2
Move disk 3 from 3 to 1
Move disk 1 from 2 to 3
Move disk 2 from 2 to 1
Move disk 1 from 3 to 1
Move disk 4 from 3 to 2
Move disk 1 from 1 to 2
Move disk 2 from 1 to 3
Move disk 1 from 2 to 3
Move disk 3 from 1 to 2
Move disk 1 from 3 to 1
Move disk 2 from 3 to 2
Move disk 1 from 1 to 2

# For help: 

./hanoi --help

```

## Contacts

Tag @evgenyrudinsky for anything related to this tool!
