/*
Copyright © 2023 NAME HERE <erudinsky@gitlab.com>
*/
package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "hanoi",
	Short: "Hanoi app helps to solve Hanoi Tower problem of any size! 🗼",
	Long: `Hanoi app helps to solve Hanoi Tower problem of any size! 🗼
 _   _                   _ 
| | | | __ _ _ __   ___ (_)
| |_| |/ _' | '_ \ / _ \| |
|  _  | (_| | | | | (_) | |
|_| |_|\__,_|_| |_|\___/|_|

For example, to find out the solution of Hanoi Tower of size 5 type the following: 

./hanoi howto --size=5

`,
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().Int("size", 3, "Size of Hanoi Tower")
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
