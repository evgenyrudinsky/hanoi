/*
Copyright © 2023 NAME HERE <erudinsky@gitlab.com>
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var howtoCmd = &cobra.Command{
	Use:   "howto",
	Short: "Shows how to solve Hanoi Tower problem of any size! 🗼",
	Long: `Shows how to solve Hanoi Tower problem of any size! 🗼

Example of use: 

./hanoi howto --size 5

`,
	Run: func(cmd *cobra.Command, args []string) {
		size, _ := cmd.Flags().GetInt("size")
		fmt.Printf("All right, to solve the problem with %v disks you would need to make the following moves: \n\n", size)
		fmt.Println(`
		
... Assuming the following (at the beggining left has all the disks): 


  1       2       3
  
  *
_____   _____   _____

		`)
		hanoi(size, 1, 2, 3)
	},
}

func hanoi(n, from, to, via int) {
	if n == 1 {
		fmt.Printf("Move disk 1 from %d to %d\n", from, to)
		return
	}
	hanoi(n-1, from, via, to)
	fmt.Printf("Move disk %d from %d to %d\n", n, from, to)
	hanoi(n-1, via, to, from)
}

func init() {
	rootCmd.AddCommand(howtoCmd)
}
